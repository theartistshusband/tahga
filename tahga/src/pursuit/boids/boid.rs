#![warn(missing_docs)]

//! # tahga::pursuit::boids::boid
//!
//! The boid module implements a single boid in the boids flocking algorithm

use nannou::color::Alpha;
use nannou::prelude::*;

/// A [`Boid`] is a single member of a flock
#[derive(Clone, Debug)]
pub struct Boid {
    /// The unique id for this boid
    id: u32,
    /// The current position
    pub position: Vec2,
    /// The current velocity
    pub velocity: Vec2,
    /// The color of this boid
    pub color: Alpha<Hsl, f32>,
    /// The range at which this boid can see other boids
    pub visual_range: f32,
    /// The max speed of this boid
    pub max_speed: f32,
    /// Not currently used
    pub max_affect: f32,
    /// Not currently used
    pub border: f32,
    /// A record of past positions
    pub path: Vec<Vec2>,
}

impl Boid {
    /// Create a new [`Boid`] instance
    ///
    /// # Arguments
    ///
    /// `id` - A unique id for this [`Boid`]
    ///
    /// `position` - The current position
    ///
    /// `velocity` - The current velocity
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.));
    /// assert_eq!(b.visual_range, 75.);
    /// assert_eq!(b.max_speed, 15.);
    /// assert_eq!(b.max_affect, 0.8);
    /// ```
    ///
    pub fn new(id: u32, position: Vec2, velocity: Vec2) -> Self {
        Boid {
            id,
            position,
            velocity,
            color: hsla(0.0, 1., 0.0, 1.0),
            visual_range: 75.,
            max_speed: 15.,
            max_affect: 0.8,
            border: 15.,
            path: Vec::new(),
        }
    }

    /// Set a single color for a [`Boid`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `color`: The color to use
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.)).color(hsla(0.7,0.5,0.1,0.3));
    ///  let c = b.color.into_components();
    /// assert_eq!(c.1, 0.5);
    /// ```
    ///
    pub fn color(mut self, color: Alpha<Hsl, f32>) -> Self {
        self.color = color;

        self
    }

    /// Set the visual_range for a [`Boid`]
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `visual_range`: the new visual_range
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.)).visual_range(10.);
    /// assert_eq!(b.visual_range, 10.);
    /// ```
    ///
    pub fn visual_range(mut self, visual_range: f32) -> Self {
        self.visual_range = visual_range;

        self
    }

    /// Set the max_speed for a [`Boid`]
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `max_speed`: the new max_speed
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.)).max_speed(10.);
    /// assert_eq!(b.max_speed, 10.);
    /// ```
    ///
    pub fn max_speed(mut self, max_speed: f32) -> Self {
        self.max_speed = max_speed;

        self
    }

    /// Set the max_affect for a [`Boid`]
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `max_affect`: the new max_affect
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.)).max_affect(10.);
    /// assert_eq!(b.max_affect, 10.);
    /// ```
    ///
    pub fn max_affect(mut self, max_affect: f32) -> Self {
        self.max_affect = max_affect;

        self
    }

    /// Set the border for a [`Boid`]
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `border`: the new border
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::pursuit::boids::boid::Boid;
    /// let b = Boid::new(0, vec2(0., 0.), vec2(10., 10.)).border(10.);
    /// assert_eq!(b.border, 10.);
    /// ```
    ///
    pub fn border(mut self, border: f32) -> Self {
        self.border = border;

        self
    }

    /// Update the position and velocity of a [`Boid`]
    ///
    /// `boids` - A reference to the Vec of all boids
    ///
    /// `rect` - The canvas rectangle
    ///
    pub fn update(&mut self, boids: &Vec<Boid>, rect: &Rect) {
        self.cohere(boids);
        self.align(boids);
        self.separate(boids);
        self.obey_speed_limit();
        self.stay_in_bounds(rect);

        self.path.push(self.position);
        if self.path.len() > 10 {
            self.path.remove(0);
        }
        self.position += self.velocity;
    }

    fn separate(&mut self, boids: &Vec<Boid>) {
        let min_distance = 20.;
        let avoidance_factor = 0.05;
        let mut acc = Vec2::ZERO;

        for other in boids {
            if self.id == other.id { continue; }
            let distance = self.position.distance(other.position);
            if distance > min_distance { continue; }
            acc += self.position - other.position;
        }

        self.velocity += acc * avoidance_factor;
    }

    fn align(&mut self, boids: &Vec<Boid>) {
        let mut avg = Vec2::ZERO;
        let alignment_factor = 0.05;
        let mut neighbors = 0;

        for other in boids {
            if self.id == other.id { continue; }
            if self.position.distance(other.position) > self.visual_range { continue; }
            avg += other.velocity;
            neighbors += 1;
        }
        if neighbors > 0 {
            avg /= neighbors as f32;
            self.velocity += (avg - self.velocity) * alignment_factor;
        }
    }

    fn cohere(&mut self, boids: &Vec<Boid>) {
        let mut center = Vec2::ZERO;
        let centering_factor = 0.005;
        let mut neighbors = 0;

        for other in boids {
            if self.id == other.id { continue; }
            if self.position.distance(other.position) > self.visual_range { continue; }
            center += other.position;
            neighbors += 1;
        }
        if neighbors > 0 {
            center /= neighbors as f32;
            self.velocity += (center - self.position) * centering_factor;
        }
    }

    fn obey_speed_limit(&mut self) {
        let speed = (self.velocity.x * self.velocity.x + self.velocity.y * self.velocity.y).sqrt();
        if speed > self.max_speed {
            self.velocity.x = (self.velocity.x / speed) * self.max_speed;
            self.velocity.y = (self.velocity.y / speed) * self.max_speed;
        }
    }

    fn stay_in_bounds(&mut self, rect: &Rect) {
        let margin = 100.;
        let turn = 1.;

        if self.position.x < rect.left() + margin {
            self.velocity.x += turn;
        }
        if self.position.x > rect.right() - margin {
            self.velocity.x -= turn;
        }
        if self.position.y < rect.bottom() + margin {
            self.velocity.y += turn;
        }
        if self.position.y > rect.top() - margin {
            self.velocity.y -= turn;
        }
    }
}