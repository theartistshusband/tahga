#![warn(missing_docs)]

//! # tahga::stroke
//!
//! A collection of lines drawn between points

pub mod line;

pub mod watercolor;
