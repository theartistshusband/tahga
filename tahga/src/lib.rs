#![warn(missing_docs)]

//! # tahga - The Artist's Husband Generative Art Library
//!
//! Useful routines for generating art images.
//!
//! This library is a work-in-progress, being put together as I experiment with building various
//! art projects in Rust.

pub mod growth;

pub mod pursuit;

pub mod stroke;

