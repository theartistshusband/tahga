#![warn(missing_docs)]

//! # tahga::growth
//!
//! The growth module implements a collection of growth algorithms

pub mod hyphae;
