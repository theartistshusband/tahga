#![warn(missing_docs)]

//! # tahga::stroke::watercolor
//!
//! The watercolor module defines WatercolorStroke, which draws a color wash for a line

use nannou::color::{Alpha, Hsl, hsla, Hsla};
use nannou::Draw;
use nannou::geom::{Vec2, vec2};
use nannou::rand::random_range;

/// WatercolorStroke defines a watercolor wash
#[derive(Debug)]
pub struct WatercolorStroke {
    /// Base color of stroke
    pub colors: Vec<Alpha<Hsl, f32>>,
    /// Brush load
    pub load: f32,
    /// Number of times to draw stoke
    pub count: u32,
    /// Stroke Type
    pub stroke_type: StrokeType,
}

impl WatercolorStroke {
    /// Create a new instance of a [`WatercolorStroke`]
    ///
    /// # Examples
    ///
    /// ```
    /// use tahga::stroke::watercolor::WatercolorStroke;
    /// let s = WatercolorStroke::new();
    /// assert_eq!(s.colors.len(), 1);
    /// assert_eq!(s.load, 100.);
    /// ```
    ///
    pub fn new() -> Self {
        Self {
            colors: vec!(hsla(0.3, 1., 0.5, 0.005)),
            load: 100.,
            count: 1,
            stroke_type: StrokeType::Full,
        }
    }

    /// Set a single color for a [`WatercolorStroke`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `color`: The color to use
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::stroke::watercolor::WatercolorStroke;
    /// let s = WatercolorStroke::new().color(hsla(0.7, 0.9, 0.3, 0.01));
    /// assert_eq!(s.colors.len(), 1);
    /// let c = s.colors[0].into_components();
    /// assert_eq!(c.1, 0.9);
    /// ```
    ///
    pub fn color(mut self, color: Alpha<Hsl, f32>) -> Self {
        self.colors = vec!(color);

        self
    }

    /// Set a list of colors for a [`WatercolorStroke`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `colors`: A Vec of colors to use
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::stroke::watercolor::WatercolorStroke;
    /// let s = WatercolorStroke::new().colors(vec!(
    ///    hsla(0.7, 0.9, 0.3, 0.01),
    ///    hsla(0.8, 0.8, 0.3, 0.01),
    ///    hsla(0.9, 0.7, 0.3, 0.01),
    ///    hsla(0.3, 0.6, 0.3, 0.01),
    /// ));
    /// assert_eq!(s.colors.len(), 4);
    /// let c = s.colors[2].into_components();
    /// assert_eq!(c.1, 0.7);
    /// ```
    ///
    pub fn colors(mut self, colors: Vec<Alpha<Hsl, f32>>) -> Self {
        if colors.len() > 0 {
            self.colors = colors;
        }

        self
    }

    /// Set the brush load for a [`WatercolorStroke`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `amount`: the new load
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::stroke::watercolor::WatercolorStroke;
    /// let s = WatercolorStroke::new().load(50.);
    /// assert_eq!(s.load, 50.);
    /// ```
    ///
    pub fn load(mut self, amount: f32) -> Self {
        self.load = amount;

        self
    }

    /// Set the stroke count for a [`WatercolorStroke`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `count`: The number times to run the stroke
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::stroke::watercolor::WatercolorStroke;
    /// let s = WatercolorStroke::new().count(4);
    /// assert_eq!(s.count, 4);
    /// ```
    ///
    pub fn count(mut self, count: u32) -> Self {
        self.count = count;

        self
    }

    /// Set the stroke count for a [`WatercolorStroke`] and return a new instance
    ///
    /// This is intended to be chained with new() in a builder pattern
    ///
    /// # Arguments
    ///
    /// `stroke_type`: The color to use
    ///
    /// # Examples
    ///
    /// ```
    /// use nannou::prelude::*;
    /// use tahga::stroke::watercolor::{WatercolorStroke, StrokeType};
    /// let s = WatercolorStroke::new().stroke_type(StrokeType::Left);
    /// assert_eq!(s.stroke_type, StrokeType::Left);
    /// ```
    ///
    pub fn stroke_type(mut self, stroke_type: StrokeType) -> Self {
        self.stroke_type = stroke_type;

        self
    }

    /// Draw that watercolor wash
    ///
    /// # Arguments
    ///
    /// `draw` - The nannou app drawing context
    ///
    /// `start` - The beginning point for the line
    ///
    /// `end` - The ending point for the line
    ///
    pub fn draw(&self, draw: &Draw, start: Vec2, end: Vec2) {
        let dist = start.distance(end);
        let dy = (end.y - start.y) / dist;
        let dx = (end.x - start.x) / dist;
        let theta = ((end.y - start.y) / (end.x - start.x)).atan();
        let rt = theta - std::f32::consts::PI / 2.; // 90 degrees to the right
        let lt = theta + std::f32::consts::PI / 2.; // 90 degrees to the left
        let mut load = self.load;
        for _ in 0..self.count {
            let mut base_color = self.random_color().into_components();
            let mut d: f32 = random_range(-std::f32::consts::PI, std::f32::consts::PI);
            for step in 0..dist.round() as u32 {
                // Point on the base line to draw from
                let p = vec2(start.x + (dx * step as f32), start.y + (dy * step as f32));
                // Choose an amplitude
                d += random_range(-0.1, 0.1);
                if d < -std::f32::consts::PI { d = -std::f32::consts::PI }
                if d > std::f32::consts::PI { d = std::f32::consts::PI }
                let amplitude = d.cos().abs() * load;
                let mut color = base_color;
                // Set the color transparency based on amplitude
                color.3 = 1. - ((1. - base_color.3) * (amplitude / load));
                // Draw the color wash
                let (begin, end) = match self.stroke_type {
                    StrokeType::Left => {
                        (p, vec2(p.x + amplitude * lt.cos(), p.y + amplitude * lt.sin()))
                    }
                    StrokeType::Right => {
                        (p, vec2(p.x + amplitude * rt.cos(), p.y + amplitude * rt.sin()))
                    }
                    StrokeType::Full => {
                        (vec2(p.x + amplitude * lt.cos(), p.y + amplitude * lt.sin()),
                         vec2(p.x + amplitude * rt.cos(), p.y + amplitude * rt.sin()))
                    }
                };
                draw.line()
                    .start(begin)
                    .end(end)
                    .color(Hsla::from_components(color));
                if amplitude < 0.1 {
                    base_color = self.random_color().into_components();
                }
            }
            load *= 0.8;
        }
    }

    fn random_color(&self) -> Alpha<Hsl, f32> {
        self.colors[random_range(0, self.colors.len())]
    }
}

/// StrokeType defines how the [`WatercolorStroke`] is drawn
#[derive(Debug, PartialEq)]
pub enum StrokeType {
    /// Draw the color wash to the left of the line
    Left,
    /// Draw the color wash to the  right side of the line
    Right,
    /// Draw the color wash on both sides of the line
    Full,
}
