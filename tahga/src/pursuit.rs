#![warn(missing_docs)]

//! # tahga::pursuit
//!
//! A collection of pursuit algorithms

pub mod boids;